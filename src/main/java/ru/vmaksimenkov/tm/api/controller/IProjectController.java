package ru.vmaksimenkov.tm.api.controller;

public interface IProjectController {

    void showList();

    void showListSorted();

    void create();

    void clear();

    void showProjectById();

    void showProjectByName();

    void showProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void updateProjectByIndex();

    void updateProjectById();

    void updateProjectByName();

    void startProjectById();

    void startProjectByName();

    void startProjectByIndex();

    void finishProjectById();

    void finishProjectByName();

    void finishProjectByIndex();

    void setProjectStatusById();

    void setProjectStatusByName();

    void setProjectStatusByIndex();

}
