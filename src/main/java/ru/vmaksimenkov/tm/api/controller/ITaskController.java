package ru.vmaksimenkov.tm.api.controller;

public interface ITaskController {

    void showList();

    void showListSorted();

    void create();

    void clear();

    void showTaskById();

    void showTaskByName();

    void showTaskByIndex();

    void showListByProjectId();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

    void updateTaskByName();

    void startTaskById();

    void startTaskByName();

    void startTaskByIndex();

    void finishTaskById();

    void finishTaskByName();

    void finishTaskByIndex();

    void setTaskStatusById();

    void setTaskStatusByName();

    void setTaskStatusByIndex();

    void bindTaskByProjectId();

    void unbindTaskById();

}
