package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}