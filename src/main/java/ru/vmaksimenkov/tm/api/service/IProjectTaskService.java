package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String projectId);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskFromProject(String taskId);

    void removeProjectById(String projectId);

    void removeProjectByName(String projectName);

    void removeProjectByIndex(Integer projectIndex);

    void clearTasks();

}
