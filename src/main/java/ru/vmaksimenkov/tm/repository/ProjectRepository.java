package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(list);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void add(Project project) {
        list.add(project);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public String getIdByName(String name) {
        for (final Project project : list) {
            if (name.equals(project.getName())) return project.getId();
        }
        return null;
    }

    @Override
    public String getIdByIndex(Integer index) {
        return list.get(index).getId();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : list) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public boolean existsById(String id) {
        for (final Project project : list) {
            if (id.equals(project.getId())) return true;
        }
        return false;
    }

    @Override
    public boolean existsByName(String name) {
        for (final Project project : list) {
            if (name.equals(project.getName())) return true;
        }
        return false;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public void remove(Project project) {
        list.remove(project);
    }

    @Override
    public void removeOneById(final String id) {
        remove(findOneById(id));
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        remove(findOneByIndex(index));
    }

    @Override
    public void removeOneByName(final String name) {
        remove(findOneByName(name));
    }

}
