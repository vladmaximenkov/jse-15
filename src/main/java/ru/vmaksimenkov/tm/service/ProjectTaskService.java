package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Task;

import static ru.vmaksimenkov.tm.util.ValidationUtil.*;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    final private ITaskRepository taskRepository;

    final private IProjectRepository projectRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (!taskRepository.existsByProjectId(projectId)) throw new TaskNotFoundException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProjectId(final String projectId, final String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId)) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        return taskRepository.bindTaskPyProjectId(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        if (isEmpty(taskId)) throw new EmptyIdException();
        return taskRepository.unbindTaskFromProject(taskId);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        taskRepository.removeAllByProjectId(projectId);
        projectRepository.removeOneById(projectId);
    }

    @Override
    public void removeProjectByName(final String projectName) {
        if (isEmpty(projectName)) throw new EmptyNameException();
        String projectId = projectRepository.getIdByName(projectName);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(projectId);
        projectRepository.removeOneByName(projectName);
    }

    @Override
    public void removeProjectByIndex(final Integer projectIndex) {
        if (!checkIndex(projectIndex, projectRepository.size())) throw new IndexIncorrectException();
        String projectId = projectRepository.getIdByIndex(projectIndex);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(projectId);
        projectRepository.removeOneByIndex(projectIndex);
    }

    @Override
    public void clearTasks() {
        taskRepository.removeAllBinded();
        projectRepository.clear();
    }

}
